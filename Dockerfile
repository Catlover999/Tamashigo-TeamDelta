FROM node:14

WORKDIR /app

COPY package.json ./

RUN yarn install

RUN yarn expo install react-native-web@~0.18.10 react-dom@18.2.0 @expo/webpack-config@^18.0.1 @react-native-async-storage/async-storage@1.17.11 react-native@0.71.8 react-native-reanimated@~2.14.4 react-native-svg@13.4.0

COPY . /app

CMD yarn run web
